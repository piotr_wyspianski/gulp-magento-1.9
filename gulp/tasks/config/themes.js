'use strict';

// NOTE - all shops name define here as seen in example
const   shopOne = 'promotors',
        shopTwo = 'shop';

// NOTE - theme and template name
// ./skin/frontend/[templateName]/[themeTemplateOne]
const   templateName = 'rwd',
        theme = 'oziway',
        separator = '_',
        themeTemplateOne = theme + separator + shopOne,
        themeTemplateTwo = theme + separator + shopTwo;

module.exports = {
    template: templateName,
    shopOne: {
        shopName: themeTemplateOne,
        name: shopOne
    },
    shopTwo: {
        shopName: themeTemplateTwo,
        name: shopTwo
    }
};