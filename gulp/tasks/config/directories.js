'use strict';
const   themes = require('./themes.js'),
        shopRoot = 'skin\\frontend\\',
        shopRootOne = shopRoot + themes.template + '\\' + themes.shopOne.shopName,
        shopRootTwo = shopRoot + themes.template + '\\' + themes.shopTwo.shopName,
        source = '\\srcTEST';


module.exports = {
    to: {
        shopOne: {
            base: shopRootOne + source,
            js: shopRootOne + source +'\\js',
            scss: shopRootOne + source + '\\scss'
        },
        shopTwo: {
            base: shopRootTwo + source,
            js: shopRootTwo + source +'\\js',
            scss: shopRootTwo + source + '\\scss'
        }
    }
};