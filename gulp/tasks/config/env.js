'use strict';
const   util = require('gulp-util');

module.exports = {
    config: {
        production: !!util.env.production,
        development: !util.env.production
    }
};