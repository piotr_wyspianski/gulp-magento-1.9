'use strict';
const   gulp = require('gulp'),
        imagemin = require('gulp-imagemin'),
        cache = require('gulp-cache'),
        path = require('../../paths.js'),
        themes = require('../config/themes.js');

gulp.task('optimize:' + themes.shopOne.name, function(){
    return gulp
        .src(path.to.shopOne.images + '/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(cache(imagemin()))
        .pipe(gulp.dest(path.to.shopOne.images +'/minified/'));
});

gulp.task('optimize:' + themes.shopTwo.name, function(){
    return gulp
        .src(path.to.shopTwo.images + '/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(cache(imagemin()))
        .pipe(gulp.dest(path.to.shopTwo.images +'/minified/'));
});

gulp.task('optimize', ['optimize:' + themes.shopOne.name,'optimize:' + themes.shopTwo.name]);