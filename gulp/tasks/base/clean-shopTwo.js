'use strict';
const   gulp = require('gulp'),
        runSequence = require('run-sequence'),
        del = require('del'),
        path = require('../../paths.js'),
        themes = require('../config/themes.js');

gulp.task('cleanJS:' + themes.shopTwo.name, function(){
    return del(path.to.shopTwo.js + '/xcbundle.js');
});

gulp.task('cleanCSS:' + themes.shopTwo.name, function(){
    return del(path.to.shopTwo.css + '/main.min.css');
});

gulp.task('cleanImages:' + themes.shopTwo.name, function(){
    return del(path.to.shopTwo.images + '/minified/');
});

gulp.task('cleanStructure:' + themes.shopTwo.name, function(){
    return del(path.to.shopTwo.src.path);
});

gulp.task('clean:' + themes.shopTwo.name, function(done){
    runSequence(
        [
            'cleanJS:' + themes.shopTwo.name,
            'cleanCSS:' + themes.shopTwo.name,
            'cleanImages:' + themes.shopTwo.name
        ],
        done
    );
});