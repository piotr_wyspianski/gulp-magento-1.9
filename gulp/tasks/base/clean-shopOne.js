'use strict';
const   gulp = require('gulp'),
        runSequence = require('run-sequence'),
        del = require('del'),
        path = require('../../paths.js'),
        themes = require('../config/themes.js');

gulp.task('cleanJS:' + themes.shopOne.name, function(){
    return del(path.to.shopOne.js + '/xcbundle.js');
});

gulp.task('cleanCSS:' + themes.shopOne.name, function(){
    return del(path.to.shopOne.css + '/main.min.css');
});

gulp.task('cleanImages:' + themes.shopOne.name, function(){
    return del(path.to.shopOne.images + '/minified/');
});

gulp.task('cleanStructure:' + themes.shopOne.name, function(){
    return del(path.to.shopOne.src.path);
});

gulp.task('clean:' + themes.shopOne.name, function(done){
    runSequence(
        [
            'cleanJS:' + themes.shopOne.name,
            'cleanCSS:' + themes.shopOne.name,
            'cleanImages:' + themes.shopOne.name
        ],
        done
    );
});