'use strict';
const   gulp = require('gulp'),
        del = require('del'),
        path = require('../../paths.js'),
        cache = require('gulp-cache');

gulp.task('clean:var', function(){
    return del(path.to.fullVar);
});

gulp.task('clean:cache', function(){
    return del(path.to.cache);
});

gulp.task('clean:imageCache', function (done) {
    return cache.clearAll(done)
});