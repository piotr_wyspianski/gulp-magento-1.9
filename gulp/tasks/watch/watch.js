'use strict';
const   gulp = require('gulp'),
        path = require('../../paths.js'),
        themes = require('../config/themes.js');

gulp.task('watch:' + themes.shopOne.name, function(){
    gulp.watch(path.to.shopOne.src.scss + '/**/*.scss', ['build-scss:' + themes.shopOne.name]);
    gulp.watch(path.to.shopOne.src.js + '/**/*.js', ['build-scripts:' + themes.shopOne.name]);
});

gulp.task('watch:' + themes.shopTwo.name, function(){
    gulp.watch(path.to.shopTwo.src.scss + '/**/*.scss', ['build-scss:' + themes.shopTwo.name]);
    gulp.watch(path.to.shopTwo.src.js + '/**/*.js', ['build-scripts:' + themes.shopTwo.name]);
});