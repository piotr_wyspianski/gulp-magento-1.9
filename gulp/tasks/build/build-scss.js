'use strict';
const   gulp = require('gulp'),
        sass = require('gulp-sass'),
        autoprefixer = require('gulp-autoprefixer'),
        rename = require('gulp-rename'),
        sourcemaps = require('gulp-sourcemaps'),
        cssnano = require('gulp-cssnano'),
        gulpif = require('gulp-if'),
        path = require('../../paths.js'),
        environment = require('../config/env.js'),
        themes = require('../config/themes.js');

gulp.task('build-scss:' + themes.shopOne.name, function () {
    gulp.src(path.to.shopOne.src.scss + '/main.scss')
        .pipe(sass({"errLogToConsole": true}).on('error', sass.logError))
        .pipe(gulpif(environment.config.development, sourcemaps.init()))
        .pipe(autoprefixer({
            "browsers":[
                "ie >= 9",
                "last 4 Edge versions",
                "last 4 ff versions",
                "last 4 Chrome versions",
                "last 4 Opera versions",
                "last 4 Safari versions",
                "ie_mob >= 10",
                "iOS >= 8",
                "android >= 4.4",
                "bb >= 10"
            ]
        }))
        .pipe(gulpif(environment.config.development, sourcemaps.write()))
        .pipe(gulpif(environment.config.production, cssnano({zindex: false})))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(path.to.shopOne.css))
});

gulp.task('build-scss:' + themes.shopTwo.name, function () {
    gulp.src(path.to.shopTwo.src.scss + '/main.scss')
        .pipe(sass({"errLogToConsole": true}).on('error', sass.logError))
        .pipe(gulpif(environment.config.development, sourcemaps.init()))
        .pipe(autoprefixer({
            "browsers":[
                "ie >= 9",
                "last 4 Edge versions",
                "last 4 ff versions",
                "last 4 Chrome versions",
                "last 4 Opera versions",
                "last 4 Safari versions",
                "ie_mob >= 10",
                "iOS >= 8",
                "android >= 4.4",
                "bb >= 10"
            ]
        }))
        .pipe(gulpif(environment.config.development, sourcemaps.write()))
        .pipe(gulpif(environment.config.production, cssnano({zindex: false})))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(path.to.shopTwo.css))
});