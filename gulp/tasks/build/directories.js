'use strict';
const   gulp = require('gulp'),
        shell = require('gulp-shell'),
        runSequence = require('run-sequence'),
        directories = require('../config/directories.js');

gulp.task('directory', function(){
    return gulp.src('*.js', {read: false})
        .pipe(shell([
            'mkdir' + ' ' + directories.to.shopOne.base + ' ' + directories.to.shopOne.js + ' ' + directories.to.shopOne.scss
        ]))
        .pipe(shell([
            'mkdir' + ' ' + directories.to.shopOne.scss + '\\base'
            + ' ' + directories.to.shopOne.scss + '\\components'
            + ' ' + directories.to.shopOne.scss + '\\helpers'
            + ' ' + directories.to.shopOne.scss + '\\layout'
            + ' ' + directories.to.shopOne.scss + '\\pages'
            + ' ' + directories.to.shopOne.scss + '\\vendor'
            + ' ' + directories.to.shopOne.scss + '\\vendor-update'
        ]));
});

gulp.task('files', function(){
    return gulp.src('*.js', {read: false})
        .pipe(shell([
            'echo ' + ' "" > ' + directories.to.shopOne.js + '\\' + 'scripts.js'
        ]))
        .pipe(shell([
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\base\\' + '_typography.scss',
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\base\\' + '_fonts.scss',
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\base\\' + '_base.scss'
        ]))
        .pipe(shell([
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\components\\' + '_buttons.scss'
        ]))
        .pipe(shell([
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\helpers\\' + '_variables.scss',
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\helpers\\' + '_mixins.scss',
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\helpers\\' + '_placeholders.scss',
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\helpers\\' + '_functions.scss'
        ]))
        .pipe(shell([
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\layout\\' + '_footer.scss',
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\layout\\' + '_header.scss'
        ]))
        .pipe(shell([
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\pages\\' + '_homepage.scss'
        ]))
        .pipe(shell([
            'echo' + ' "//Download from https://necolas.github.io/normalize.css/" > ' + directories.to.shopOne.scss + '\\vendor\\' + '_normalize.scss'
        ]))
        .pipe(shell([
            'echo' + ' "//File for override vendor files" > ' + directories.to.shopOne.scss + '\\vendor-update\\' + '_normalize.scss'
        ]))
        .pipe(shell([
            'echo' + ' "" > ' + directories.to.shopOne.scss + '\\main.scss'
        ]))
});

gulp.task('structure', function(done){
    runSequence(
        [
            'directory'
        ],
        [
            'files'
        ],
        done
    );
});