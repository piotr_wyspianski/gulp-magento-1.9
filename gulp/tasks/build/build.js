'use strict';
const   gulp = require('gulp'),
        runSequence = require('run-sequence'),
        themes = require('../config/themes.js');

gulp.task('build:'+themes.shopOne.name, function(done){
    return runSequence('clean:'+themes.shopOne.name, ['build-scss:'+themes.shopOne.name, 'build-scripts:'+themes.shopOne.name], done);
});

gulp.task('build:'+themes.shopTwo.name, function(done){
    return runSequence('clean:'+themes.shopTwo.name, ['build-scss:'+themes.shopTwo.name, 'build-scripts:'+themes.shopTwo.name, 'optimize:'+themes.shopTwo.name], done);
});

gulp.task('build:all', function(done){
    return runSequence([
            'clean:'+themes.shopTwo.name,
            'clean:'+themes.shopOne.name
        ],
        [
            'build-scss:'+themes.shopTwo.name,
            'build-scripts:'+themes.shopTwo.name,
            'build-scss:'+themes.shopOne.name,
            'build-scripts:'+themes.shopOne.name,
            'optimize'
        ],
            done
    );
});