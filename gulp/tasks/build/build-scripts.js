'use strict';
const   gulp = require('gulp'),
        browserify = require('browserify'),
        source = require('vinyl-source-stream'),
        buffer = require('vinyl-buffer'),
        uglify = require('gulp-uglify'),
        rename = require('gulp-rename'),
        gulpif = require('gulp-if'),
        path = require('../../paths.js'),
        environment = require('../config/env.js'),
        themes = require('../config/themes.js');

gulp.task('build-scripts:'+themes.shopOne.name, function(){
    return browserify(path.to.shopOne.src.js + '/scripts.js')
        .transform('babelify',{
            presets: ['es2015', 'react']
        })
        .bundle()
        .pipe(source('scripts.js'))
        .pipe(buffer())
        .pipe(gulpif(environment.config.production, uglify({ mangle: false })))
        .pipe(rename('xcbundle.js'))
        .pipe(gulp.dest(path.to.shopOne.js));
});

gulp.task('build-scripts:'+themes.shopTwo.name, function(){
    return browserify(path.to.shopTwo.src.js + '/scripts.js')
        .transform('babelify',{
            presets: ['es2015', 'react']
        })
        .bundle()
        .pipe(source('scripts.js'))
        .pipe(buffer())
        .pipe(gulpif(environment.config.production, uglify({ mangle: false })))
        .pipe(rename('xcbundle.js'))
        .pipe(gulp.dest(path.to.shopTwo.js));
});