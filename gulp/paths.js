'use strict';
const   gulp = require('gulp'),
        themes = require('./tasks/config/themes.js'),
        shopRoot = './skin/frontend/',
        shopRootOne = shopRoot + themes.template + '/' + themes.shopOne.shopName,
        shopRootTwo = shopRoot + themes.template + '/' + themes.shopTwo.shopName,
        source = '/src';


module.exports = {
    to: {
        cache: './var/cache/',
        fullVar: './var/',
        shopOne: {
            base: shopRootOne,
            js: shopRootOne + '/js',
            css: shopRootOne + '/css',
            images: shopRootOne + '/images',
            src: {
                path: shopRootOne + source,
                js: shopRootOne + source +'/js',
                scss: shopRootOne + source + '/scss'
            }
        },
        shopTwo: {
            base: shopRootTwo,
            js: shopRootTwo + '/js',
            css: shopRootTwo + '/css',
            images: shopRootTwo + '/images',
            src: {
                path: shopRootTwo + source,
                js: shopRootTwo + source +'/js',
                scss: shopRootTwo + source + '/scss'
            }
        }
    }
};

gulp.task('print', function(){
    console.log(shopRootOne + source);
    console.log(themes.template);
});