const   gulp = require('gulp'),
        themes = require('./gulp/tasks/config/themes.js'),
        path = require('./gulp/paths.js'),
        requireDir  = require('require-dir');

requireDir('./gulp/tasks', { recurse: true });

gulp.task('default', function() {
    console.log('\x1b[33m%s\x1b[0m','Gulp common commands');
    console.log('\x1b[31m%s\x1b[0m','[--production]','\x1b[0m',' - option flag for production mode. Minifing files, no sourcemaps.');
    // Clear final files and destinations and tha create new, actual depends of shop and for all defined shops
    console.log('\x1b[32m%s', 'gulp build:' + themes.shopOne.name + '\x1b[0m','\x1b[31m', '[--production]','\x1b[0m','- build for ' + '\x1b[35m' + path.to.shopOne.base + '\x1b[0m');
    console.log('\x1b[32m%s', 'gulp build:' + themes.shopTwo.name + '\x1b[0m','\x1b[31m', '[--production]','\x1b[0m',' - build for ' + '\x1b[35m' + path.to.shopTwo.base + '\x1b[0m');
    console.log('\x1b[32m%s', 'gulp build:all','\x1b[0m','\x1b[31m', '[--production]','\x1b[0m','- build for ' + themes.shopOne.name + ' and ' + themes.shopTwo.name);
    //build scss and javascript for shopOne
    console.log('\x1b[32m%s', 'gulp build-scss:' + themes.shopOne.name + '\x1b[0m','- build scss files from ' + '\x1b[35m' + path.to.shopOne.src.scss + '\x1b[0m' + ' and copy (optimized) main.min.css file to: ' + '\x1b[35m' + path.to.shopOne.css +'\x1b[0m');
    console.log('\x1b[32m%s', 'gulp build-scripts:' + themes.shopOne.name + '\x1b[0m','- build javascripts scripts files from ' + '\x1b[35m' + path.to.shopOne.src.js + '\x1b[0m' + ' and copy (optimized) xcbundle.js file to: ' + '\x1b[35m' + path.to.shopOne.js + '\x1b[0m');
    //build scss and javascript for shopTwo
    console.log('\x1b[32m%s', 'gulp build-scss:' + themes.shopTwo.name + '\x1b[0m','- build scss files from ' + '\x1b[35m' + path.to.shopTwo.src.scss + '\x1b[0m' + ' and copy (optimized) main.min.css file to: ' + '\x1b[35m' + path.to.shopTwo.css +'\x1b[0m');
    console.log('\x1b[32m%s', 'gulp build-scripts:' + themes.shopTwo.name + '\x1b[0m','- build javascripts scripts files from ' + '\x1b[35m' + path.to.shopTwo.src.js + '\x1b[0m' + ' and copy (optimized) xcbundle.js file to: ' + '\x1b[35m' + path.to.shopTwo.js + '\x1b[0m');
    // Watchers scss and javascript depends of shop
    console.log('\x1b[32m%s', 'gulp watch:' + themes.shopOne.name + '\x1b[0m','- watcher for js and css files in ' + '\x1b[35m' + path.to.shopOne.src.path + '\x1b[0m');
    console.log('\x1b[32m%s', 'gulp watch:' + themes.shopTwo.name + '\x1b[0m','- watcher for js and css files in ' + '\x1b[35m' + path.to.shopTwo.src.path + '\x1b[0m');
    // Optimize images and copy to minified folder depends of shop
    console.log('\x1b[32m%s', 'gulp optimize:' + themes.shopOne.name + '\x1b[0m','- optimize all image files in ' + '\x1b[35m' + path.to.shopOne.images + '\x1b[0m' + ' and copy optimized files to: ' + '\x1b[35m' + path.to.shopOne.images + '/minified' + '\x1b[0m');
    console.log('\x1b[32m%s', 'gulp optimize:' + themes.shopTwo.name + '\x1b[0m','- watcher for js and css files in ' + '\x1b[35m' + path.to.shopTwo.images + '\x1b[0m' + ' and copy optimized files to: ' + '\x1b[35m' + path.to.shopTwo.images + '/minified' + '\x1b[0m');
    // Global Magento clearing: entire var folder, only cache, cache for image optimizer
    console.log('\x1b[32m%s', 'gulp clean:var','\x1b[0m',' - delete entire folder ' + '\x1b[35m' +  './var' + '\x1b[0m');
    console.log('\x1b[32m%s', 'gulp clean:cache','\x1b[0m',' - delete only cache ' + '\x1b[35m' + './var/cache' + '\x1b[0m');
    console.log('\x1b[32m%s', 'gulp clean:imageCache','\x1b[0m',' - delete image cache for gulp-imagemin');
});